% f = [matrixV matrixM FRONT CROWDING_DISTANCE] = populasi tapi sudah
% diisi bagian matrixM nya.
function populasi = evaluasiIndividu(populasi, M, P, hargaIkan, kebutuhanIkan, posisiSekarang, pelabuhan, jarakAntarPelabuhan, maksimalPembelian)

% membaca matrixV untuk dijadikan input populasi pada masing2 fungsi
% matrixV = kromosom tanpa fitness, front, dan crowding distance

jumlahJenisIkan = size(kebutuhanIkan,1);
V = P + jumlahJenisIkan + (P * jumlahJenisIkan);
matrixV = populasi(:,1:V);

% Fungsi-fungsi evaluasi:
evaluasi(1).func = @() hitungHarga(matrixV, P, hargaIkan, kebutuhanIkan, jumlahJenisIkan);
evaluasi(2).func = @() hitungJarakHaversine(matrixV, P, jarakAntarPelabuhan);
evaluasi(3).func = @() hitungKelebihanAlokasi(matrixV, P, jumlahJenisIkan, maksimalPembelian);
%evaluasi(3).func = @() hitungKedekatanPelabuhan(matrixV, P, pelabuhan);

%eksekusi fungsi-fungsi evaluasi yang diperlukan
for m = 1:M
   populasi(:,V+m) = round(evaluasi(m).func());
end

populasi(:,V+M+1:end) = 0;

function harga = hitungHarga(populasi, P, hargaIkan, kebutuhanIkan, jumlahJenisIkan)
[jumlahKromosom jumlahGen] = size(populasi);
matrixHarga = zeros(jumlahKromosom, P, jumlahJenisIkan);

genPelabuhan = populasi(:, 1:P);
genJenisIkan = populasi(:, P+1 : P + jumlahJenisIkan);
genDistribusiPembelian = populasi(:,P + jumlahJenisIkan + 1 : end);
distribusiPembelian = reshape(genDistribusiPembelian, jumlahKromosom, jumlahJenisIkan, P);

%membuat matrix harga komoditas di setiap pelabuhan
hargaLokal = zeros(jumlahKromosom, jumlahJenisIkan, P);

for i=1:jumlahKromosom
    for j=1:P
        %i
        %j
        %genPelabuhan(i,j)
        %genJenisIkan(i,:)
          hargaLokal(i,:,j) = hargaIkan(genPelabuhan(i,j),genJenisIkan(i,:));
    end
end

matrixHarga= hargaLokal .* distribusiPembelian;
%mengembalikan ke 2D
matrixHarga = reshape(matrixHarga, jumlahKromosom, P*jumlahJenisIkan);

harga = sum(matrixHarga')';


% HITUNG AKUMULASI JARAK HAVERSINE KE PELABUHAN TERPILIH (dalam kromosom)
function jarak = hitungJarakHaversine(populasi, P, jarakAntarPelabuhan)
[jumlahKromosom jumlahGen] = size(populasi);
jarak = zeros(jumlahKromosom,1);
genPelabuhan = populasi(:, 1:P);
for i = 1:jumlahKromosom
   g = genPelabuhan(i,:);   % Memperoleh id pelabuhan
   x = jarakAntarPelabuhan(g);  % Memperoleh jarak2 ke pelabuhan2 terpilih
   jarak(i) = sum(x);   % Hitung akumulasi jarak ke masing2 pelabuhan
end


% HITUNG KELEBIHAN ALOKASI
function hasil = hitungKelebihanAlokasi(populasi, P, jumlahJenisIkan, maksimalPembelian)
% populasi
% P
% jumlahJenisIkan
% maksimalPembelian
pop = size(populasi,1);
alokasi = populasi(:,P+jumlahJenisIkan+1:P+jumlahJenisIkan+(P*jumlahJenisIkan));
kapasitas = zeros(pop,P*jumlahJenisIkan);
for i=1:pop
    kapasitas(i,:) = reshape(( maksimalPembelian(populasi(i,1:P),populasi(i,P+1:P+ jumlahJenisIkan)))',1,[]);  % untuk memperoleh kapasitas pelabuhan berdasarkan pelabuhan n jenis ikan
end
% kapasitas
selisih = kapasitas-alokasi;    % menghitung selisih antara kapasitas dan alokasi
z=ceil(normc(-1 * selisih));    % Yang alokasinya melebihi kapasitas pelabuhan, diberi nilai 1, sedangkan yang tidak melebihi diberi nilai 1
hasil = sum(z')';    % Akumulasi alokasi pembelian yang melebihi kapasitas. Semakin mendekati 0 semakin baik (yang melebihi kapasitas semakin sedikit).

% HITUNG KEDEKATAN
function kedekatan = hitungKedekatanPelabuhan(populasi, P, pelabuhan, jumlahJenisIkan)
[jumlahKromosom jumlahGen] = size(populasi);
kedekatan = zeros(jumlahKromosom,1);
genPelabuhan = populasi(:, 1:P)

for i = 1:jumlahKromosom
   %x merupakan matrix berisi koordinat kumpulan beberapa pelabuhan
   x = pelabuhan(genPelabuhan(i,:)',:)
   xMin = min(x);
   xMax = max(x);
   
   % menghitung jarak euclid antara titik2 terluar
   kedekatan(i) = sqrt((xMax-xMin)*(xMax-xMin)');
end