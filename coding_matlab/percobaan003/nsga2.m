% CONTOH: nsga2( 10, 100, 2, 2, kebutuhanIkan, posisiSekarang, hargaIkan, pelabuhan, maksimalPembelianIkan)
% M = jumlah objective function
% P = jumlah pelabuhan maksimal
function hasil = nsga2( pop, jumlahGenerasi, M, P, kebutuhanIkan, posisiSekarang, hargaIkan, pelabuhan, maksimalPembelian )


%figure;

pc = 0.9;                        %peluang crossover
pm = 1/size(kebutuhanIkan,1);    %peluang mutasi dibuat 1/jumlahJenisIkan
V = P + size(kebutuhanIkan,1) + (P * size(kebutuhanIkan,1)); %jumlah decision variable
jarakKePelabuhan = hitungJarakKePelabuhan(posisiSekarang, pelabuhan); % Menghitung jarak posisiSekarang ke semua Pelabuhan
% jarakAntarPelabuhan = hitungJarakHaversine(pelabuhan); % Menghitung jarak antar pelabuhan

%%menentukan axis untuk plotting
% maxHarga = max(hargaIkan,[],1);
% minHarga = min(hargaIkan,[],1);

%% inisialisasi populasi
populasi = inisialisasi(pop, M, P, pelabuhan, kebutuhanIkan);

%% evaluasi individu
populasi = evaluasiIndividu(populasi, M, P, hargaIkan, kebutuhanIkan, posisiSekarang, pelabuhan, jarakKePelabuhan, maksimalPembelian);

%% nonDominationSorting
populasi = nonDominationSorting(populasi, M, V);

jenisIkan = size(kebutuhanIkan, 1); %jumlah jenis ikan yang dibutuhkan
jumlahPelabuhan = size(pelabuhan,1);
m = size(populasi,2); % panjang kromosom (jumlah gen)

% Menentukan titik2 yang di crossover jika tp diketahui
% cp = crossover point
for n=1:(P+jenisIkan)
    temp = zeros(1,m);
    if n<=P %jika titik randomnya berupa gen pelabuhan
        temp(1, n+1:end) = 1;
        %find(temp)
    else %jika titik randomnya berupa gen jenis ikan
        temp2 = [];
        for p=1:P
            temp2 = [temp2 n+(jenisIkan*p)];
        end
        temp(1, temp2) = 1;
        %find(temp)
    end
    cp(n).points = find(temp);
end

for i = 1 : jumlahGenerasi
    % pool - size of the mating pool. It is common to have this to be half the
    %        population size.
    % tour - Tournament size. Original NSGA-II uses a binary tournament
    %        selection, but to see the effect of tournament size this is kept
    %        arbitary, to be choosen by the user.
    pool = round(pop/2);
    tour = 2;

    induk = tournament_selection(populasi, pool, tour);

    offspringChromosome = operator_genetik(induk, M, P, pc, pm, kebutuhanIkan, jumlahPelabuhan, cp);
    % evaluasi offspringChromosome    
    offspringChromosome = evaluasiIndividu(offspringChromosome, M, P, hargaIkan, kebutuhanIkan, posisiSekarang, pelabuhan, jarakKePelabuhan, maksimalPembelian) ;
    
    % menggabungkan induk dengan anak
    intermediateChromosome = [populasi; offspringChromosome];
    intermediateChromosome(:,end-1:end) = 0;    % bagian front dan crowding distance di nol kan
    
    %% Hapus Kromosom Kembar Identik untuk menjaga keragamanan
    intermediateChromosome = unique(intermediateChromosome, 'rows');
    
    %% hapus individu yang pelabuhannya sama karena 
    % TIDAK BOLEH ADA PELABUHAN YANG SAMA DALAM SATU KROMOSOM (SOLUSI)
    intermediateChromosome = hapusDuplikatPelabuhan(intermediateChromosome, P);
    

    %% hapus kromosom yang alokasi pembeliannya melebihi kapasitas pelabuhan
    % --- edit dulu di bagian evaluasi
    % bimbingan dulu untuk bagian ini
    % -----------------------------------
    
    %% non domination sorting
    intermediateChromosome = nonDominationSorting(intermediateChromosome, M, V);
    populasi = replace_chromosome(intermediateChromosome, M, V, pop);
end

hasil=populasi;