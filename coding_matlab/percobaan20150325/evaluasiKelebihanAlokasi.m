% HITUNG KELEBIHAN ALOKASI
function hasil = evaluasiKelebihanAlokasi(populasi, P, jumlahJenisIkan, maksimalPembelian)
% populasi
% P
% jumlahJenisIkan
% maksimalPembelian
pop = size(populasi,1);
alokasi = populasi(:,P+jumlahJenisIkan+1:P+jumlahJenisIkan+(P*jumlahJenisIkan));
kapasitas = zeros(pop,P*jumlahJenisIkan);
for i=1:pop
    kapasitas(i,:) = reshape(( maksimalPembelian(populasi(i,1:P),populasi(i,P+1:P+ jumlahJenisIkan)))',1,[]);  % untuk memperoleh kapasitas pelabuhan berdasarkan pelabuhan n jenis ikan
end
% kapasitas
selisih = kapasitas-alokasi;    % menghitung selisih antara kapasitas dan alokasi
z=ceil(normc(-1 * selisih));    % Yang alokasinya melebihi kapasitas pelabuhan, diberi nilai 1, sedangkan yang tidak melebihi diberi nilai 1
hasil = sum(z')';    % Akumulasi alokasi pembelian yang melebihi kapasitas. Semakin mendekati 0 semakin baik (yang melebihi kapasitas semakin sedikit).
