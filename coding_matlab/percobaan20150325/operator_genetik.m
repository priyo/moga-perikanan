% jumlahJenisIkan = banyaknya jenis ikan yang dibutuhkan
% cp = crossover point
% P = jumlah maksimal pelabuhan
% jumlahPelabuhan = jumlah semua pelabuhan yang mungkin dipilih
function anak = operator_genetik(induk, M, P, pc, pm, kebutuhanIkan, jumlahPelabuhan, cp)
[N,m] = size(induk);
jenisIkan = size(kebutuhanIkan,1);

anak = [];

for i = 1:N
   %berdasarkan peluang crossover
   if rand(1) < pc
      %inisialisasi asal anak = 0
      anak_1 = [];
      anak_2 = [];
      
      %memilih acak induk_1 dan induk_2
      index_induk_1 = ceil(N*rand(1));
      index_induk_2 = ceil(N*rand(1));
      
      %memastikan induk terpilih tidak identik
      while isequal(index_induk_1, index_induk_2)
          index_induk_2 = ceil(N*rand(1));
      end
      
      %mendapatkan nilai kromosom dari induk terpilih
      induk_1 = induk(index_induk_1,:);
      induk_2 = induk(index_induk_2,:);
      
      %menentukan titik potong acak dan melakukan pindah silang
      % P+jenisIkan adalah pointer untuk crossover
      TP = ceil((P+jenisIkan) * rand(1));
      
      anak_1 = induk_1;
      anak_2 = induk_2;
      anak_1(cp(TP).points) = induk_2(cp(TP).points);
      anak_2(cp(TP).points) = induk_1(cp(TP).points);
      
      anak = [anak; anak_1; anak_2];
   end
   
   % mutasi
   if rand(1) < pm
       
      %memilih acak induk_3
      index_induk_3 = ceil(N*rand(1));
      %mendapatkan nilai kromosom dari induk terpilih
      induk_3 = induk(index_induk_3,:);
      anak_3 = induk_3;
      %menentukan titik mutasi acak dan melakukan pindah silang
      TM = ceil((P+jenisIkan) * rand(1));
      
        if TM<=P %jika titik randomnya berupa gen pelabuhan
            anak_3(TM) = ceil(jumlahPelabuhan * rand(1));
        else %jika titik randomnya berupa gen jenis ikan
            idKebutuhanIkan = TM - P; % kebutuhan jenis ikan ke ...
            rand1 = rand(1,P);
            randomAlokasiPembelian = round(rand1 ./ sum(rand1) * kebutuhanIkan(idKebutuhanIkan,2)); % alokasi diacak untuk jenis ikan X
            anak_3(cp(TM).points) = randomAlokasiPembelian;
        end

      anak = [anak; anak_3];
   end
end
anak(:,P+jenisIkan+(P*jenisIkan)+1:end) = 0;

end