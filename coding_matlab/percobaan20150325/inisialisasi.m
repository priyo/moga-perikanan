% Fungsi untuk membuat populasi

% INPUT:
% pop: ukuran populasi (contoh: 50)
% M: ukuran objective function
% P: jumlah maksimal jumlah pelabuhan sebagai lokasi pembelian
% matrixPelabuhan: matrix pelabuhan berisi lat-lon, disini hanya untuk
%   mengetahui jumlah pelabuhannya saja
% kebutuhanIkan: matrix berisi kolom ID jenis ikan dan jumlah ikan

% OUTPUT:
% populasi: matrix berukuran m x (a + b + (a x b) + M + 2) atau m x (V + M + 2)
% V = a + b + (a x b) = decision variable
%   m: ukuran populasi (banyaknya individu/ kromosom) = pop
%   a: jumlah gen berupa ID pelabuhan
%   b: jumlah gen berupa ID jenis ikan
%   a x b : digunakan untuk menyimpan kombinasi pembelian ikan di setiap
%       pelabuhan
%   M: ukuran objective function
%   2 lokus terakhir untuk rank dan crowding distance

% Contoh output, a = 2, b = 3, berarti V = 2 + 3 + (2 x 3) = 11 :
%   populasi = 
%   [   x1 x2 y1 y2 y3 j1 j2 j3 j4 j5 j6 0 0 0 0; 
%       x3 x4 y1 y2 y3 j7 j8 j9 j10 j11 j12 0 0 0 0; ]
%   artinya untuk kromosom pertama: 
%       pada pelabuhan x1 membeli ikan 
%           jenis y1 sebanyak j1,
%           jenis y2 sebanyak j2,
%           jenis y3 sebanyak j3,
%       pada pelabuhan x2 membeli ikan 
%           jenis y1 sebanyak j4,
%           jenis y2 sebanyak j5,
%           jenis y3 sebanyak j6,
%                   
%   untuk kromosom kedua:  
%       pada pelabuhan x3 membeli ikan 
%           jenis y1 sebanyak j7,
%           jenis y2 sebanyak j8,
%           jenis y3 sebanyak j9,
%       pada pelabuhan x4 membeli ikan 
%           jenis y1 sebanyak j10,
%           jenis y2 sebanyak j11,
%           jenis y3 sebanyak j12,

function populasi = inisialisasi(pop, M, P, matrixPelabuhan, kebutuhanIkan)

jumlahPelabuhan = size(matrixPelabuhan,1);  % semua pelabuhan
jumlahJenisIkan = size(kebutuhanIkan,1);    % jumlah jenis ikan yg dibutuhkan

%gen pelabuhan tempat pembelian ikan
random1 = rand(pop, P);
subPopulasi1 = ceil(random1 * jumlahPelabuhan);

%gen jenis ikan untuk setiap pelabuhan
subPopulasi2 = repmat(kebutuhanIkan(:,1)', pop, 1);

%gen alokasi jumlah setiap jenis ikan pada masing-masing pelabuhan
random3 = rand(pop, jumlahJenisIkan, P);
pembagi = sum(random3, 3);
pembagi = repmat(pembagi, 1, P);

r3 = [];
for i=1:P
   r3 = [r3 random3(:,:,i)];
end

kebutuhanRep = repmat(kebutuhanIkan(:,2)',pop,P);
pengali = r3./pembagi;
subPopulasi3 = round(kebutuhanRep.*pengali);

%subPopulasi digabungkan
populasi = [subPopulasi1 subPopulasi2 subPopulasi3 zeros(pop,M+2)];