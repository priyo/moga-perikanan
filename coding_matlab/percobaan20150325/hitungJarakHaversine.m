% menghitung jarak antar node dengan format lat,lon
function matrixJarak = hitungJarakHaversine(input)

jumlahNode = size(input,1);

matrixJarak = zeros(jumlahNode);

for i = 1:jumlahNode
   for j = i:jumlahNode
       matrixJarak(i,j) = haversine(input(i,:),input(j,:));
       matrixJarak(j,i) = matrixJarak(i,j);
   end
end