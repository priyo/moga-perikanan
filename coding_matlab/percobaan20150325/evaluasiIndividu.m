% f = [matrixV matrixM FRONT CROWDING_DISTANCE] = populasi tapi sudah
% diisi bagian matrixM nya.
function populasi = evaluasiIndividu(populasi, M, P, hargaIkan, kebutuhanIkan, posisiSekarang, pelabuhan, jarakKePelabuhan, maksimalPembelian)

% membaca matrixV untuk dijadikan input populasi pada masing2 fungsi
% matrixV = kromosom tanpa fitness, front, dan crowding distance

jumlahJenisIkan = size(kebutuhanIkan,1);
V = P + jumlahJenisIkan + (P * jumlahJenisIkan);
matrixV = populasi(:,1:V);

% Fungsi-fungsi evaluasi:
evaluasi(1).func = @() evaluasiHarga(matrixV, P, hargaIkan, kebutuhanIkan, jumlahJenisIkan);
evaluasi(2).func = @() evaluasiJarak(matrixV, P, jarakKePelabuhan);
evaluasi(3).func = @() evaluasiKelebihanAlokasi(matrixV, P, jumlahJenisIkan, maksimalPembelian);

%eksekusi fungsi-fungsi evaluasi yang diperlukan
for m = 1:M
   populasi(:,V+m) = round(evaluasi(m).func());
end

populasi(:,V+M+1:end) = 0;
