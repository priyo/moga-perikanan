% menghitung jarak dari satu titik ke semua pelabuhan
% contoh : hitungJarakKePelabuhan([15 130], matrixPelabuhan) 
% matrix pelabuhannya lat,lon
function matrixJarak = hitungJarakKePelabuhan(posisiSekarang, matrixPelabuhan)

    jumlahPelabuhan = size(matrixPelabuhan,1);

    jarak = zeros(jumlahPelabuhan,1);

    for i = 1:jumlahPelabuhan
       matrixJarak(i,1) = haversine(posisiSekarang,matrixPelabuhan(i,:));
    end
end