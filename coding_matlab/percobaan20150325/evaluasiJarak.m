% HITUNG AKUMULASI JARAK HAVERSINE KE PELABUHAN TERPILIH (dalam kromosom)
function jarak = evaluasiJarak(populasi, P, jarakKePelabuhan)
[jumlahKromosom] = size(populasi, 1);
jarak = zeros(jumlahKromosom,1);
genPelabuhan = populasi(:, 1:P);
for i = 1:jumlahKromosom
   g = unique(genPelabuhan(i,:));   % Memperoleh id pelabuhan yang uniq saja
   x = jarakKePelabuhan(g);  % Memperoleh jarak2 ke pelabuhan2 terpilih
   jarak(i) = sum(x);   % Hitung akumulasi jarak ke masing2 pelabuhan
end