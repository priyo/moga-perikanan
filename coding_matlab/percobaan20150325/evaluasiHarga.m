function harga = evaluasiHarga(populasi, P, hargaIkan, kebutuhanIkan, jumlahJenisIkan)
[jumlahIndividu jumlahGen] = size(populasi);
matrixHarga = zeros(jumlahIndividu, P, jumlahJenisIkan);

genPelabuhan = populasi(:, 1:P);
genJenisIkan = populasi(:, P+1 : P + jumlahJenisIkan);
genDistribusiPembelian = populasi(:,P + jumlahJenisIkan + 1 : end);
distribusiPembelian = reshape(genDistribusiPembelian, jumlahIndividu, jumlahJenisIkan, P);

%membuat matrix harga komoditas di setiap pelabuhan
hargaLokal = zeros(jumlahIndividu, jumlahJenisIkan, P);

for i=1:jumlahIndividu
    for j=1:P
        hargaLokal(i,:,j) = hargaIkan(genPelabuhan(i,j),genJenisIkan(i,:));
    end
end

matrixHarga = hargaLokal .* distribusiPembelian;

harga = sum(matrixHarga(:));
