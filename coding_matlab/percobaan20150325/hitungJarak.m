% menghitung jarak antar node dengan format x,y
function matrixJarak = hitungJarak(input)

jumlahNode = size(input,1);

matrixJarak = zeros(jumlahNode);

for i = 1:jumlahNode
   for j = 1:jumlahNode
       matrixJarak(i,j) = sqrt((input(i,1)-input(j,1))^2 + (input(i,2)-input(j,2))^2)
   end
end