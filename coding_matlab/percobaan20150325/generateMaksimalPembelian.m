% jumlahVariasi bisa disamakan dengan jumlahPelabuhan
% jumlahKomoditas = banyaknya jenis ikan
% hasil berupa matrix random antara 100 - 1000
function harga = generateMaksimalPembelian(jumlahVariasi, jumlahKomoditas)

random = round(rand(jumlahVariasi,jumlahKomoditas) * 90) *10;

harga = 100 + random;