% P = jumlah pelabuhan
function populasi = hapusDuplikatPelabuhan(populasi, P)
    genPelabuhan = populasi(:, 1:P);
    [jumlahKromosom jumlahPelabuhan] = size(genPelabuhan);
    % Menentukan gen yang akan dihapus krn ada pelabuhan yg sama
    jmlPelabuhan(1:jumlahKromosom,1) = jumlahPelabuhan;
    for i=1:jumlahKromosom        
        g = genPelabuhan(i,:);
        jumlahPelabuhanUnik(i,1) = size(unique(g),2);
    end
    pelabuhanDuplikat = jmlPelabuhan - jumlahPelabuhanUnik;
    delete_index = find(pelabuhanDuplikat);
    populasi(delete_index,:) = [];