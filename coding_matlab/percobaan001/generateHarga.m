% jumlahVariasi bisa disamakan dengan jumlahPelabuhan
% jumlahKomoditas = banyaknya jenis ikan
function harga = generateHarga(jumlahVariasi, jumlahKomoditas)

hargaDasar = rand(1,jumlahKomoditas) * 100;
harga = zeros(jumlahVariasi, jumlahKomoditas);

for i = 1:jumlahVariasi
    random = rand(1,jumlahKomoditas) * 10;
    harga(i,:) =  hargaDasar + random;
end