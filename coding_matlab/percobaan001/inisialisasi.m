% Fungsi untuk membuat populasi

% INPUT:
% jumlahKromosom: banyaknya kromosom yang akan dibuat
% matrixPelabuhan: matrix pelabuhan berisi lat-lon, disini hanya untuk
%   mengetahui jumlah pelabuhannya saja
% kebutuhanIkan: matrix berisi kolom ID jenis ikan dan jumlah ikan

% OUTPUT:
% populasi: matrix berukuran m x (a + b)
%   m: jumlah kromosom dalam populasi
%   a: jumlah gen berupa ID pelabuhan
%   b: jumlah gen berupa ID jenis ikan
%   a = b

% Contoh output:
%   populasi = [x1 x2 x3 y1 y2 y3; x4 x5 x6 y4 y5 y6]
%   artinya: 
%       pada pelabuhan x1 membeli ikan y1, 
%       pada pelabuhan x2 membeli ikan y2, 
%       dst ...

function populasi = inisialisasi(jumlahKromosom, matrixPelabuhan, kebutuhanIkan)

jumlahPelabuhan = size(matrixPelabuhan,1);
jumlahJenisIkan = size(kebutuhanIkan,1);

%gen pelabuhan tempat pembelian ikan
random1 = rand(jumlahKromosom, jumlahJenisIkan);
subPopulasi1 = ceil(random1 * jumlahPelabuhan);

%gen jenis ikan untuk setiap pelabuhan
subPopulasi2 = zeros(jumlahKromosom, jumlahJenisIkan);
for i = 1:jumlahJenisIkan
    subPopulasi2(:,i) = kebutuhanIkan(i,1);
end

%subPopulasi digabungkan
populasi = [subPopulasi1 subPopulasi2];