% f = [harga totalJarak kedekatanAntarPelabuhan, FRONT, CROWDING DISTANCE]
function f = evaluasiIndividu(populasi, hargaIkan, kebutuhanIkan, posisiSekarang, pelabuhan, jarakAntarPelabuhan)

f(:,1) = hitungHarga(populasi, hargaIkan, kebutuhanIkan);
f(:,2) = hitungJarakEuclid(populasi, posisiSekarang, pelabuhan);
f(:,3) = hitungKedekatanPelabuhan(populasi, pelabuhan);
%f(:,4) = zeros(size(populasi,1),1);
%f(:,5) = zeros(size(populasi,1),1);

% di minuskan semua karena mencari nilai minimal
%f = -f;

function harga = hitungHarga(populasi, hargaIkan, kebutuhanIkan)
[jumlahKromosom jumlahGen] = size(populasi);
jumlahJenisIkan = jumlahGen/2;  
matrixHarga = zeros(jumlahKromosom, jumlahJenisIkan);

genPelabuhan = populasi(:, 1:jumlahJenisIkan);
genJenisIkan = populasi(:, jumlahJenisIkan+1:jumlahGen);

for i = 1:jumlahKromosom
    for j = 1:jumlahJenisIkan
        matrixHarga(i,j) = hargaIkan(genPelabuhan(i,j),genJenisIkan(i,j));
    end
end

harga = sum(matrixHarga')';

function jarak = hitungJarakEuclid(populasi, posisiSekarang, pelabuhan)
[jumlahKromosom jumlahGen] = size(populasi);
jumlahJenisIkan = jumlahGen/2; 
jarak = zeros(jumlahKromosom,1);
x1 = posisiSekarang(1);
y1 = posisiSekarang(2);

genPelabuhan = populasi(:, 1:jumlahJenisIkan);

for i = 1:jumlahKromosom
   pelabuhanUnik = unique(genPelabuhan(i,:)); %menghindari perhitungan ganda pada satu pelabuhan
   
   for j = 1:size(pelabuhanUnik,2)
       x2 = pelabuhan(pelabuhanUnik(j),1);
       y2 = pelabuhan(pelabuhanUnik(j),2);
       jarak(i) = jarak(i) + sqrt((x1-x2)^2 + (y1-y2)^2);
   end
end


function kedekatan = hitungKedekatanPelabuhan(populasi, pelabuhan)
[jumlahKromosom jumlahGen] = size(populasi);
jumlahJenisIkan = jumlahGen/2; 
kedekatan = zeros(jumlahKromosom,1);
genPelabuhan = populasi(:, 1:jumlahJenisIkan);

for i = 1:jumlahKromosom
   x = [];  %x merupakan matrix berisi koordinat kumpulan beberapa pelabuhan
   for j = 1:jumlahJenisIkan
       x = [x ; pelabuhan(genPelabuhan(i,j),:)];    
   end
   
   % menghitung jarak titik2 terluar
   kedekatan(i) =  sqrt(( max(x(:,1))-min(x(:,1)) )^2 + ( max(x(:,2))-min(x(:,2)) )^2);
end