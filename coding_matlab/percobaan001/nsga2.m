% CONTOH: nsga2( 20, 10, kebutuhanIkan, posisiSekarang, hargaIkan, pelabuhan, jarakAntarPelabuhan )
function hasil = nsga2( jumlahKromosom, jumlahGenerasi, kebutuhanIkan, posisiSekarang, hargaIkan, pelabuhan, jarakAntarPelabuhan )

pc = 0.9;                        %peluang crossover
pm = 1/size(kebutuhanIkan,1);    %peluang mutasi dibuat 1/jumlahJenisIkan
M = 3;                           %jumlah objective function
V = size(kebutuhanIkan,1)*size(kebutuhanIkan,2); %jumlah decision variable

% inisialisasi populasi
populasi = inisialisasi(jumlahKromosom, pelabuhan, kebutuhanIkan);

hasilEvaluasi = evaluasiIndividu(populasi, hargaIkan, kebutuhanIkan, posisiSekarang, pelabuhan, jarakAntarPelabuhan);
x = [populasi hasilEvaluasi];

%variabel kedua menunjukkan jumlah objective function
hasilSorting = nonDominationSorting(x, M, V);

for i = 1 : jumlahGenerasi
    % pool - size of the mating pool. It is common to have this to be half the
    %        population size.
    % tour - Tournament size. Original NSGA-II uses a binary tournament
    %        selection, but to see the effect of tournament size this is kept
    %        arbitary, to be choosen by the user.
    pool = round(jumlahKromosom/2);
    tour = 2;

    induk = tournament_selection(hasilSorting, pool, tour);
    %induk diambil nilai V nya saja (decision variable)
    induk = induk(:,1:V);

    offspring_chromosome = operator_genetik(induk, M, V, pc, pm, size(pelabuhan,1));
    intermediate_chromosome = [hasilSorting(:,1:V); offspring_chromosome];

    % evaluasi intermediate_chromosome
    hasilEvaluasi = evaluasiIndividu(intermediate_chromosome, hargaIkan, kebutuhanIkan, posisiSekarang, pelabuhan, jarakAntarPelabuhan);
    x = [intermediate_chromosome hasilEvaluasi];

    %non domination sorting
    hasilSorting = nonDominationSorting(x, M, V);
    hasilSorting = replace_chromosome(hasilSorting, M, V, jumlahKromosom);

    fprintf('generasi ke %d\n',i);
    %if ~mod(i,100)
    %    clc
    %    fprintf('%d generations completed\n',i);
    %end
    
    %% Visualize
    % The following is used to visualize the result if objective space
    % dimension is visualizable.
    if M == 2
        %plot(hasilSorting(:,V + 1),hasilSorting(:,V + 2),'*');
    elseif M ==3
        dataPlot = hasilSorting;
        
        plot3(dataPlot(:,V + 1),dataPlot(:,V + 2),dataPlot(:,V + 3),'*');
        title('Visualisasi Hasil NSGA')
        xlabel('harga')
        ylabel('jarak')
        zlabel('kedekatan')
        %view(0,0)
    end
    pause(1);

end
hasilSorting

%% Result
% Save the result
save solution.txt hasilSorting


