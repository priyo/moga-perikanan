% Fungsi untuk membuat populasi

% INPUT:
% pop: ukuran populasi (contoh: 50)
% M: ukuran objective function
% matrixPelabuhan: matrix pelabuhan berisi lat-lon, disini hanya untuk
%   mengetahui jumlah pelabuhannya saja
% kebutuhanIkan: matrix berisi kolom ID jenis ikan dan jumlah ikan

% OUTPUT:
% populasi: matrix berukuran m x (a + b + M + 2) atau m x (V + M + 2)
% V = a + b = decision variable
%   m: ukuran populasi (banyaknya individu/ kromosom) = pop
%   a: jumlah gen berupa ID pelabuhan
%   b: jumlah gen berupa ID jenis ikan
%   a = b
%   2 lokus terakhir untuk rank dan crowding distance

% Contoh output, V = 6, M=2 :
%   populasi = [x1 x2 x3 y1 y2 y3 0 0 0 0; x4 x5 x6 y4 y5 y6 0 0 0 0]
%   artinya: 
%       pada pelabuhan x1 membeli ikan y1, 
%       pada pelabuhan x2 membeli ikan y2, 
%       dst ...
%       y1 = y4, y2 = y5, y3 = 6, dst

function populasi = inisialisasi(pop, M, matrixPelabuhan, kebutuhanIkan)

jumlahPelabuhan = size(matrixPelabuhan,1);
jumlahJenisIkan = size(kebutuhanIkan,1);

%gen pelabuhan tempat pembelian ikan
random1 = rand(pop, jumlahJenisIkan);
subPopulasi1 = ceil(random1 * jumlahPelabuhan);

%gen jenis ikan untuk setiap pelabuhan
subPopulasi2 = zeros(pop, jumlahJenisIkan);
for i = 1:jumlahJenisIkan
    subPopulasi2(:,i) = kebutuhanIkan(i,1);
end

%subPopulasi digabungkan
populasi = [subPopulasi1 subPopulasi2 zeros(pop,M+2)];