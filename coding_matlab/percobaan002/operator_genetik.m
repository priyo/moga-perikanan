function anak = operator_genetik(induk, M, V, pc, pm, jumlahPelabuhan)

[N,m] = size(induk);
jenisIkan = V/2;

p=1;

%penanda proses genetika
was_crossover = 0;
was_mutation = 0;
anak = [];

for i = 1:N
   %berdasarkan peluang crossover
   if rand(1) < pc
      %inisialisasi asal anak = 0
      anak_1 = [];
      anak_2 = [];
      
      %memilih acak induk_1
      index_induk_1 = ceil(N*rand(1));
      index_induk_2 = ceil(N*rand(1));
      
      %memastikan induk terpilih tidak identik
      while isequal(index_induk_1, index_induk_2)
          index_induk_2 = ceil(N*rand(1));
      end
      
      %mendapatkan nilai kromosom dari induk terpilih
      induk_1 = induk(index_induk_1,:);
      induk_2 = induk(index_induk_2,:);
      
      %menentukan titik potong acak dan melakukan pindah silang
      TP = ceil(jenisIkan * rand(1));
      anak_1 = [induk_1(1:TP) induk_2(TP+1:m)];
      anak_2 = [induk_2(1:TP) induk_1(TP+1:m)];
      
      %memberi tanda bahwa telah terjadi proses crossover
      was_crossover = 1;
      was_mutation = 0;
      
   %berdasarkan peluang mutasi
   else
      %memilih induk secara acak
      index_induk_3 = ceil(N * rand(1));
      %anak mendapatkan nilai kromosom dari induk terpilih
      induk_3 = induk(index_induk_3,:);
      
      %proses mutasi
      for j=1:jenisIkan
         r = rand(1);
         if r < pm
            temp = induk_3(j);
            while isequal(temp, induk_3(j))
                temp = ceil(rand(1)*jumlahPelabuhan);               
            end
            induk_3(j) = temp;
         end
      end
      anak_3 = induk_3;
      
      %memberi tanda bahwa telah terjadi mutasi
      was_crossover = 0;
      was_mutation = 1;
   end
   
   %menggabungkan anak hasil crossover dan mutasi
   if was_crossover
       anak(p,:) = anak_1;
       anak(p+1,:) = anak_2;
       was_crossover = 0;
       p = p+2;
   elseif was_mutation
       anak(p,:) = anak_3;
       was_mutation = 0;
       p = p+1;
   end
end