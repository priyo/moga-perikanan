% f = [matrixV matrixM FRONT CROWDING_DISTANCE] = populasi tapi sudah
% diisi bagian matrixM nya.
function populasi = evaluasiIndividu(populasi, M, V, hargaIkan, kebutuhanIkan, posisiSekarang, pelabuhan, jarakAntarPelabuhan)

% membaca matrixV untuk dijadikan input populasi pada masing2 fungsi

matrixV = populasi(:,1:V);

% Fungsi-fungsi evaluasi:
evaluasi(1).func = @() hitungHarga(matrixV, hargaIkan, kebutuhanIkan);
evaluasi(2).func = @() hitungJarakEuclid(matrixV, posisiSekarang, pelabuhan);
evaluasi(3).func = @() hitungKedekatanPelabuhan(matrixV, pelabuhan);

%eksekusi fungsi-fungsi evaluasi yang diperlukan
for m = 1:M
   populasi(:,V+m) = evaluasi(m).func();
end

populasi(:,V+m+1:end) = 0;

function harga = hitungHarga(populasi, hargaIkan, kebutuhanIkan)
[jumlahKromosom jumlahGen] = size(populasi);
jumlahJenisIkan = jumlahGen/2;  
matrixHarga = zeros(jumlahKromosom, jumlahJenisIkan);

genPelabuhan = populasi(:, 1:jumlahJenisIkan);
genJenisIkan = populasi(:, jumlahJenisIkan+1:jumlahGen);

for i = 1:jumlahKromosom
    for j = 1:jumlahJenisIkan
        matrixHarga(i,j) = hargaIkan(genPelabuhan(i,j),genJenisIkan(i,j));
    end
end

harga = sum(matrixHarga')';

function jarak = hitungJarakEuclid(populasi, posisiSekarang, pelabuhan)
[jumlahKromosom jumlahGen] = size(populasi);
jumlahJenisIkan = jumlahGen/2; 
jarak = zeros(jumlahKromosom,1);
x1 = posisiSekarang(1);
y1 = posisiSekarang(2);

genPelabuhan = populasi(:, 1:jumlahJenisIkan);

for i = 1:jumlahKromosom
   pelabuhanUnik = unique(genPelabuhan(i,:)); %menghindari perhitungan ganda pada satu pelabuhan
   
   for j = 1:size(pelabuhanUnik,2)
       x2 = pelabuhan(pelabuhanUnik(j),1);
       y2 = pelabuhan(pelabuhanUnik(j),2);
       jarak(i) = jarak(i) + sqrt((x1-x2)^2 + (y1-y2)^2);
   end
end
%mengubah derajat menjadi km
jarak = jarak*111.12;

function kedekatan = hitungKedekatanPelabuhan(populasi, pelabuhan)
[jumlahKromosom jumlahGen] = size(populasi);
jumlahJenisIkan = jumlahGen/2; 
kedekatan = zeros(jumlahKromosom,1);
genPelabuhan = populasi(:, 1:jumlahJenisIkan);

for i = 1:jumlahKromosom
   x = [];  %x merupakan matrix berisi koordinat kumpulan beberapa pelabuhan
   for j = 1:jumlahJenisIkan
       x = [x ; pelabuhan(genPelabuhan(i,j),:)];    
   end
   
   % menghitung jarak titik2 terluar
   kedekatan(i) =  sqrt(( max(x(:,1))-min(x(:,1)) )^2 + ( max(x(:,2))-min(x(:,2)) )^2);
end