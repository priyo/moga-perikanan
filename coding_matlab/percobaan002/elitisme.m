function anakelit = elitisme(populasi)
%populasi = anakmutasi, anak hasil mutasi
jumlahpopulasi = size(populasi,1);

%% perhitungan fitness function memanggil fungsi fitness
nilaifitness = fitness(populasi);

%% find maximum minimum fitness
[nilai_max index_max] = max(nilaifitness);
[nilai_min index_min] = min(nilaifitness);

%% replace chromosome 
 populasi(index_min, :) = populasi(index_max,:);
 anakelit = populasi;