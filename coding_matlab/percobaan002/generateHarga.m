% jumlahVariasi bisa disamakan dengan jumlahPelabuhan
% jumlahKomoditas = banyaknya jenis ikan
function harga = generateHarga(jumlahVariasi, jumlahKomoditas)

hargaDasar = round( rand(1,jumlahKomoditas) * 100);
hargaDasar = repmat(hargaDasar,jumlahVariasi,1);
random = round(rand(jumlahVariasi,jumlahKomoditas) * 10);

harga = hargaDasar + random;