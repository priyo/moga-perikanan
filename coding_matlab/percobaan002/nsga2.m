% CONTOH: nsga2( 20, 10, kebutuhanIkan, posisiSekarang, hargaIkan, pelabuhan, jarakAntarPelabuhan )
% M = jumlah objective function
function hasil = nsga2( pop, jumlahGenerasi, M, kebutuhanIkan, posisiSekarang, hargaIkan, pelabuhan, jarakAntarPelabuhan )
tic
figure;

pc = 0.9;                        %peluang crossover
pm = 1/size(kebutuhanIkan,1);    %peluang mutasi dibuat 1/jumlahJenisIkan
V = size(kebutuhanIkan,1)*size(kebutuhanIkan,2); %jumlah decision variable

%%menentukan axis untuk plotting
% maxHarga = max(hargaIkan,[],1);
% minHarga = min(hargaIkan,[],1);

%% inisialisasi populasi
populasi = inisialisasi(pop, M, pelabuhan, kebutuhanIkan);

%% evaluasi individu
populasi = evaluasiIndividu(populasi, M, V, hargaIkan, kebutuhanIkan, posisiSekarang, pelabuhan, jarakAntarPelabuhan);

%% nonDominationSorting
populasi = nonDominationSorting(populasi, M, V);

for i = 1 : jumlahGenerasi
    % pool - size of the mating pool. It is common to have this to be half the
    %        population size.
    % tour - Tournament size. Original NSGA-II uses a binary tournament
    %        selection, but to see the effect of tournament size this is kept
    %        arbitary, to be choosen by the user.
    pool = round(pop/2);
    tour = 2;

    induk = tournament_selection(populasi, pool, tour);

    offspringChromosome = operator_genetik(induk, M, V, pc, pm, size(pelabuhan,1));
    % evaluasi offspringChromosome
    offspringChromosome = evaluasiIndividu(offspringChromosome, M, V, hargaIkan, kebutuhanIkan, posisiSekarang, pelabuhan, jarakAntarPelabuhan);
    
    % membuat intermediateChromosome
    intermediateChromosome = [populasi; offspringChromosome];
    
    % hapus individu duplikat
    intermediateChromosome = unique(intermediateChromosome, 'rows');

    %non domination sorting
    intermediateChromosome = nonDominationSorting(intermediateChromosome, M, V);
    populasi = replace_chromosome(intermediateChromosome, M, V, pop);

    if ~mod(i,10)
        % Visualize
        % The following is used to visualize the result if objective space
        % dimension is visualizable.
        if M == 2                      
            % untuk selain class 1
            notClass1 = populasi(populasi(:, M+V+1) ~= 1, :);
            plot(notClass1(:,V+1),notClass1(:,V+2), ...
                'x');
            title('Visualisasi Hasil NSGA');
            xlabel('harga (x Rp 1000)');
            ylabel('jarak (km)');
            axis([220 240 0 2000]);
            
            hold on;          
            
            % untuk class 1            
            class1 = populasi(populasi(:, M+V+1) == 1, :);
            class1 = class1(:, V+1:V+2);
            class1 = sortrows(class1);
            plot(class1(:,1),class1(:,2), ...
                'r.',...
                'MarkerFaceColor','r');

            hold off;
        elseif M == 3
            notClass1 = populasi(populasi(:, M+V+1) ~= 1, :);
            plot3(notClass1(:,V + 1),notClass1(:,V + 2),notClass1(:,V + 3),'x');
            title('Visualisasi Hasil NSGA');
            xlabel('harga');
            ylabel('jarak');
            zlabel('kedekatan');            
            axis([220 240 0 250 0 120]);
            view(45,20)
            
            hold on;          
            
            % membuat garis untuk class 1            
            class1 = populasi(populasi(:, M+V+1) == 1, :);
            class1 = class1(:, V+1:V+3);
            plot3(class1(:,1),class1(:,2),class1(:,3),'r.','MarkerFaceColor','r');

            hold off;
        end
        fprintf('generasi ke %d\n',i);
        pause(0.3);
    end

end
hasil = populasi;
toc


